import React, {useCallback, useState} from 'react';
import { storiesOf } from '@storybook/react';
import {select, withKnobs} from '@storybook/addon-knobs';

const inputTypes = [
    'color',
    'date',
    'email',
    'number',
    'password',
    'text'
];

function TextInputExample()
{
    const [value, setValue] = useState('Dummy example');
    
    const onChange = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
        setValue(event.currentTarget.value);
    }, []);
    
    return <div>
        <div style={{ margin: '1rem 0' }}>Current value: {value}</div>
        <input type={select('Input Type', inputTypes, 'text')} value={value} onChange={onChange} />
    </div>;
}

storiesOf('Example', module)
    .addDecorator(withKnobs)
    .add('Input example', () => <TextInputExample />, {
        notes: `
            # Some example notes
            
            Hello there!
        `
    });