import 'reflect-metadata';
import {unmountComponentAtNode} from 'react-dom';
import React from 'react';
import * as ReactDOM from 'react-dom';

let appElement = document.getElementById('app-container');

if (!appElement)
{
    appElement = document.createElement('div');
    appElement.id = 'app-container';
    appElement.classList.add('app');
    document.body.appendChild(appElement);
}
else
{
    unmountComponentAtNode(appElement);
}

ReactDOM.render(<div>Hello there</div>, appElement);